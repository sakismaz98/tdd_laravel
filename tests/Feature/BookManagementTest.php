<?php

namespace Tests\Feature;

use App\Author;
use App\Book;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class BookManagementTest extends TestCase
{
	use RefreshDatabase;

	/**
	 *  @test
	 */
	public function a_book_can_be_added_to_the_library()
	{
		$response = $this->post('/books', $this->data());

		$book = Book::first();

		$this->assertCount(1, Book::all());

		$response->assertRedirect($book->path());
	}

	/**
	 *  @test
	 */
	public function a_title_is_required()
	{
		$response = $this->post('/books', array_merge($this->data(), ['title' => '']));

		$response->assertSessionHasErrors('title');
	}

	/**
	 *  @test
	 */
	public function a_author_is_required()
	{
		$response = $this->post('/books', array_merge($this->data(), ['author_id' => '']));

		$response->assertSessionHasErrors('author_id');
	}

	/**
	 *  @test
	 */
	public function a_book_can_be_updated()
	{
		// Post A book
		$this->post('/books', $this->data());

		// Get The book
		$book = Book::first();

		// Update A Book
		$response = $this->patch($book->path(), [
			'title' => 'New Title',
			'author_id' => 'New Author',
		]);

		// Test if book updated successfull
		$this->assertEquals('New Title', Book::first()->title);
		$this->assertEquals(2, Book::first()->author_id);

		$response->assertRedirect($book->fresh()->path());
	}

	/**
	 *  @test
	 */
	public function a_book_can_be_deleted()
	{
		// Post A book
		$this->post('/books', $this->data());

		// Get The book
		$book = Book::first();

		// Prove That book was on d
		$this->assertCount(1, Book::all());

		// Update A Book
		$response = $this->delete($book->path());

		// Test if book updated successfull
		$this->assertCount(0, Book::all());
		$response->assertRedirect('/books');
	}

	/** @test */
	public function a_new_author_is_automatically_added()
	{
		// Post A book
		$this->post('/books', $this->data());

		$book = Book::first();
		$author = Author::first();

		$this->assertEquals($author->id, $book->author_id);
		$this->assertCount(1, Author::all());
	}

	private function data()
	{
		return [
			'title' => 'Cool Book Title',
			'author_id' => 'Sakis',
		];
	}
}
